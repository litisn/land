package org.land.admin.mapper;

import org.land.admin.entity.DictIndex;
import org.land.common.base.BaseMapper;

/**
 * <p>
 * 系统数据字典索引信息 Mapper 接口
 * </p>
 *
 * @Author lee.
 * @since 2019-02-27
 */
public interface DictIndexMapper extends BaseMapper<DictIndex> {

}
