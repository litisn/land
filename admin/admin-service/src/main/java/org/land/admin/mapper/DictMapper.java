package org.land.admin.mapper;

import org.land.admin.entity.Dict;
import org.land.common.base.BaseMapper;

/**
 * <p>
 * 系统数据字典明细信息 Mapper 接口
 * </p>
 *
 * @Author lee.
 * @since 2019-02-27
 */
public interface DictMapper extends BaseMapper<Dict> {

}
