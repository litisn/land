package org.land.admin.mapper;

import org.land.admin.entity.User;
import org.land.common.base.BaseMapper;

/**
 * <p>
 * 系统用户信息 Mapper 接口
 * </p>
 *
 * @Author lee.
 * @since 2019-02-27
 */
public interface UserMapper extends BaseMapper<User> {

}
