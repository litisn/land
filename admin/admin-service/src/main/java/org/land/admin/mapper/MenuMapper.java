package org.land.admin.mapper;

import org.land.admin.entity.Menu;
import org.land.common.base.BaseMapper;

/**
 * <p>
 * 系统菜单信息 Mapper 接口
 * </p>
 *
 * @Author lee.
 * @since 2019-02-27
 */
public interface MenuMapper extends BaseMapper<Menu> {

}
