package org.land.admin.mapper;

import org.land.admin.entity.Dept;
import org.land.common.base.BaseMapper;

/**
 * <p>
 * 系统部门信息 Mapper 接口
 * </p>
 *
 * @Author lee.
 * @since 2019-02-27
 */
public interface DeptMapper extends BaseMapper<Dept> {

}
