package org.land.admin.mapper;

import org.land.admin.entity.Holiday;
import org.land.common.base.BaseMapper;

/**
 * <p>
 * 系统角色信息 Mapper 接口
 * </p>
 *
 * @Author lee.
 * @since 2019-02-27
 */
public interface HolidayMapper extends BaseMapper<Holiday> {

}
